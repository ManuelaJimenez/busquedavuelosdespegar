Feature: Buscar vuelos economicos
  Yo como viajero recurrente
  Quiero realizar una busqueda de vuelos baratos a cartagena
  Para mis vacaciones

  @CasoExitoso
  Scenario: Busqueda exitosa
    Given el usuario esta en la pagina de Despegar
    And el usuario da click en cerrar ventana Beneficios y click en Vuelos
    When el usuario ingresa la ciudades origen "Medellin, Colombia"
    And el usuario ingresa la ciudades destino "Cartagena de Indias, Bolivar, Colombia"
    And el usuario ingresa fechas del vuelo
    And el usuario ingresa numero de pasajeros
    And busca los vuelos
    And Selecciona los diez primer vuelo y lo copia en un excel
    Then se listan las opciones de los vuelos

  @CasoFallidoOrigenIgualAlDesino
  Scenario: Busqueda fallida ciudades iguales en las busqueda
    Given el usuario esta en la pagina de Despegar
    And el usuario da click en cerrar ventana Beneficios y click en Vuelos
    When el usuario ingresa la ciudades origen "Medellin, Colombia"
    And el usuario ingresa la ciudades destino "Medellin, Colombia"
    And el usuario ingresa fechas del vuelo
    And el usuario ingresa numero de pasajeros
    And busca los vuelos
    Then debe salir mensaje de error El destino debe ser diferente del orige

  @CasoFallidoOrigenCaracteresEspeciales
  Scenario: Busqueda fallida ciudad Caracteres especiales
    Given el usuario esta en la pagina de Despegar
    And el usuario da click en cerrar ventana Beneficios y click en Vuelos
    When el usuario ingresa la ciudades origen "#$%"
    And el usuario ingresa la ciudades destino "Cartagena de Indias, Bolivar, Colombia"
    And el usuario ingresa fechas del vuelo
    And el usuario ingresa numero de pasajeros
    And busca los vuelos
    Then debe salir mensaje de error ingresa un origen

  @CasoFallidoOrigenVacio
  Scenario: Busqueda fallida Fecha Origen Vacia
    Given el usuario esta en la pagina de Despegar
    And el usuario da click en cerrar ventana Beneficios y click en Vuelos
    When el usuario ingresa la ciudades origen ""
    And el usuario ingresa la ciudades destino "Cartagena de Indias, Bolivar, Colombia"
    And el usuario ingresa fechas del vuelo
    And el usuario ingresa numero de pasajeros
    And busca los vuelos
    Then debe salir mensaje de error ingresa un origen

  @CasoFallidoFechaRegresoMenor
  Scenario: Busqueda fallida Fecha Regreso Menor
    Given el usuario esta en la pagina de Despegar
    And el usuario da click en cerrar ventana Beneficios y click en Vuelos
    When el usuario ingresa la ciudades origen "Medellin, Colombia"
    And el usuario ingresa la ciudades destino "Cartagena de Indias, Bolivar, Colombia"
    And el usuario ingresa fecha fin menor que fecha inicial
    And el usuario ingresa numero de pasajeros
    And busca los vuelos
    Then debe poner de nuevo la fecha salida en blanco
    

