
Programa: Para realizara pruebas de B�squeda de los 10 vuelos mas econ�micos en la Pagina Despegar 
_______________________________________________________________________________________________________________________________________________________________________

Descripci�n Generar:
Esta aplicaci�n permite la b�squeda exitosa de los 10 vuelos m�s econ�micos entre dos destinos. Generando la lista de los mismos en un Excel y se�alando en color VERDE el m�s econ�mico.

Tiene casos prueba de negativos, como:
 *Ciudad Origen y Destino iguales
 *Ingreso de Ciudades con caracteres especiales
 *Fecha de Origen Vac�a
 *Fecha de Regreso inferior a la fecha de salida

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
Especificaciones T�cnicas: 

Framework: Cucumber
Compilador: JAVA Gradle
Patrones desarrollo: BDD
Herramienta de automatizaci�n utilizada: IDE de Eclipse - Selenium
Estrategia de automatizaci�n: 
	1. Se naveg� inicialmente en la p�gina como usuario, para validar los casos de prueba que se deb�an generar y conocer la p�gina Despegar.
	2. Se procedi� a escribir las Historias de Usuario en BDD con notaciones en Gherkin. 
	3. Se inici� con la automatizaci�n caso por caso y haciendo las respectivas pruebas

El navegador y la versi�n utilizada: Google Chrome / Versi�n 66.0.3359.181 (Build oficial) (64 bits)

----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Las conclusiones de la automatizaci�n:
 	1. Se pone en practica lo aprendido en el curso en Ceiba
	2. Se busca ir automatizando y probando paso por paso
	3. La automatizacion logra buscar los 10 vuelos mas economicos entre Medellin y Cartagena, en Septiembre para dos pasajeros. al final los registra en un Excel          marcando el mas economico en verde
