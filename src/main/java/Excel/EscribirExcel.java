package Excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class EscribirExcel {

	public void writeExcel(String[] dataToWrite) throws IOException {

		// Create an object of File class to open xlsx file

		File file = new File("D:\\precios.xlsx");

		// Create an object of FileInputStream class to read excel file

		FileInputStream inputStream = new FileInputStream(file);

		XSSFWorkbook Libro = new XSSFWorkbook(inputStream);

		// Read excel sheet by sheet name

		Sheet hoja = Libro.getSheet("Hoja1");

		// Create a loop over the cell of newly created Row

		for (int j = 0; j < dataToWrite.length; j++) {

			// Fill data in row
			Row nuevafila = hoja.createRow(j);

			Cell cell = nuevafila.createCell(0);

			cell.setCellValue(dataToWrite[j]);

			
			if (j == 0) {
				
				XSSFCellStyle style = Libro.createCellStyle();
				style.setFillForegroundColor(HSSFColor.GREEN.index);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(style); // se a�ade el style crea anteriormente
			}

		}

		// Close input stream

		inputStream.close();

		// Create an object of FilearchivoSalida class to create write data in excel
		// file

		FileOutputStream archivoSalida = new FileOutputStream(file);

		// write data in the excel file

		Libro.write(archivoSalida);

		// close output stream

		archivoSalida.close();

	}

}
