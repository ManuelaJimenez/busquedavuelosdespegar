package pages;

import java.util.ArrayList;

import javax.rmi.CORBA.Util;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BusquedaPage {

	static WebDriver driver;

	// Constructor: interaccion entre las clases
	public BusquedaPage(WebDriver driver) {
		this.driver = driver;
	}

    public void esperarSegundos(WebDriver driver, int segundos){
		      synchronized(driver){
		         try {
		            driver.wait(segundos * 1000);
		         } catch (InterruptedException e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		         }
		      }
		      
		   
		   } 

	public void cerrarVentanaAprovechaBeneficios() {
		esperarSegundos(driver, 4);
		driver.findElement(By.xpath("/html/body/div[16]/div/div[1]/span")).click();
	}

	public void clickEnVuelos() {
		esperarSegundos(driver, 4);
		driver.findElement(By.xpath("//*[@id=\"incentive-popup\"]/span[1]")).click();
		driver.findElement(By.xpath("//span[text()=\"Vuelos\"]")).click();
	}

	public void ingresarOrigen(String origen) throws InterruptedException {
		driver.findElement(By.xpath("//input[@placeholder=\"Ingresa desde d�nde viajas\" and @class=\"input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-origin-input sbox-primary sbox-places-first places-inline\"]")).clear();
		driver.findElement(By.xpath("//input[@placeholder=\"Ingresa desde d�nde viajas\" and @class=\"input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-origin-input sbox-primary sbox-places-first places-inline\"]")).sendKeys(Keys.BACK_SPACE);
		driver.findElement(By.xpath("//input[@placeholder=\"Ingresa desde d�nde viajas\" and @class=\"input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-origin-input sbox-primary sbox-places-first places-inline\"]")).sendKeys(origen);
		esperarSegundos(driver, 4);
		driver.findElement(By.xpath("//input[@placeholder=\"Ingresa desde d�nde viajas\" and @class=\"input-tag sbox-main-focus sbox-bind-reference-flight-roundtrip-origin-input sbox-primary sbox-places-first places-inline\"]")).sendKeys(Keys.ENTER);
	}

	public void ingresarDestino(String destino) {
		driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div/input")).clear();
		driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div/input")).sendKeys(destino);
		esperarSegundos(driver, 4);
		driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div/input")).sendKeys(Keys.ENTER);
		esperarSegundos(driver, 4);
	}

	public void ingresarFechaSalida() {
        driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/input")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div[2]/i")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div[2]/i")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div[2]/i")).click();
        esperarSegundos(driver, 4);
        driver.findElement(By.xpath("/html/body/div[4]/div/div[4]/div[5]/div[4]/span[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/input")).click();
	} 

	public void ingresarFechaRegreso() {
	    esperarSegundos(driver, 4);
		driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/input")).click();
	        driver.findElement(By.xpath("/html/body/div[4]/div/div[4]/div[5]/div[4]/span[29]")).click();
        driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/input")).click();
    }

	public void ingresaPasajeros() {
		driver.findElement(By.xpath("//*[@id='searchbox-sbox-all-boxes']/div/div/div/div[3]/div[2]/div[1]/div[2]/div[2]/div[6]/div[2]/div/input")).click();
		esperarSegundos(driver, 4);
        driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div[2]/div/div[1]/div/div[1]/div[2]/div/a[2]")).click(); 
	}

	public void clickFecha() {
		driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[4]/div/a")).click();
		
	}

	public static String ErrorCiudadesIguales() {
		return driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div/span[2]")).getText(); 
	}

	public static String ErrorCiudadesCaracteresEspeciales() {
		return driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[1]/div[1]/div/div[1]/div/div/div/span")).getText(); 
	}

	public void ingresarFechaRegresoErrada() {
		esperarSegundos(driver, 4);
		driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[4]/input")).click();
		esperarSegundos(driver, 4);
		driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div[1]/i")).click();
		esperarSegundos(driver, 4);
        driver.findElement(By.xpath("/html/body/div[4]/div/div[4]/div[4]/div[4]/span[24]")).click();
        driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div/div/div/div[3]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/input")).click();
    }
	
	public static String FechaSalidaBlanco() {
		return driver.findElement(By.xpath("//*[@id=\"searchbox-sbox-all-boxes\"]/div[2]/div/div/div[3]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/span[2]")).getText(); 
	}
	
}
	
