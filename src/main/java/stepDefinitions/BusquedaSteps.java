package stepDefinitions;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.http.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.BusquedaPage;
import pages.ResultadoVuelosPage;


import static org.junit.Assert.*;

public class BusquedaSteps {

	WebDriver driver;
	BusquedaPage busquedapage;
	ResultadoVuelosPage resultadovuelopage;

	@Before // se ejecuta antes de cada escenario
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		driver = new ChromeDriver();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("test-type");
		options.addArguments("start-maximized");
		options.addArguments("disable-infobars");
		options.addArguments("--disable-extensions"); 
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		busquedapage = new BusquedaPage(driver);
		resultadovuelopage = new ResultadoVuelosPage(driver);
	} 

	 @After
	 public void tearDown() {
	 driver.quit();
	 }

	@Given("^el usuario esta en la pagina de Despegar$")
	public void usuarioEstaEnPaginaDespegar() {
		driver.get("https://www.despegar.com.co");
	}

	@When("el usuario da click en cerrar ventana Beneficios y click en Vuelos")
	public void usuarioIngresaCiudades() throws InterruptedException {
		busquedapage.cerrarVentanaAprovechaBeneficios();
		busquedapage.clickEnVuelos();
	}

	@And("^el usuario ingresa la ciudades origen \"(.*)\"$")
	public void ciudadOrigen (String origen) throws InterruptedException {
		busquedapage.ingresarOrigen(origen);
	}
	
	@And("^el usuario ingresa la ciudades destino \"(.*)\"$")
	public void ciudadDestino (String destino) {
		busquedapage.ingresarDestino(destino);
	}
	
	@And("el usuario ingresa fechas del vuelo")
	public void usuarioIngresaFecha() {
		busquedapage.ingresarFechaSalida();
		busquedapage.ingresarFechaRegreso();
	}
	
	@And("el usuario ingresa fecha fin menor que fecha inicial")
	public void usuarioIngresaFechaErrada() {
		busquedapage.ingresarFechaSalida();
		busquedapage.ingresarFechaRegresoErrada();
	}
	

	@And("el usuario ingresa numero de pasajeros")
	public void usuarioIngresaNumPasajeros() {
		busquedapage.ingresaPasajeros();
	}

	@And("^busca los vuelos$")
	public void buscaLosVuelos() {
		busquedapage.clickFecha();
	}

	@And("Selecciona los diez primer vuelo y lo copia en un excel")
	public void seleccionarVueloseleccionar10PreciosBaratos(){
		resultadovuelopage.seleccionar10PreciosBaratos();
	}
	
	@Then("^se listan las opciones de los vuelos$")
	public void opcionesVuelos() {
		String result = ResultadoVuelosPage.opcionesVuelos();
		assertEquals("Despegar.com . Resultados de Vuelos", result);
	}
	
	@Then("^debe salir mensaje de error El destino debe ser diferente del orige$")
	public void errorCiudadesIguales() {
		String result = BusquedaPage.ErrorCiudadesIguales();
		assertEquals("El destino debe ser diferente del origen", result);
	}
	
	@Then("^debe salir mensaje de error ingresa un origen$")
	public void errorCiudadesCaracteresEspeciales() {
		String result = BusquedaPage.ErrorCiudadesCaracteresEspeciales();
		assertEquals("Ingresa un origen", result);
	}
	
	@Then("^debe poner de nuevo la fecha salida en blanco$")
	public void FechaSalidaBlanco() {
		String result = BusquedaPage.FechaSalidaBlanco();
		assertEquals("Ingresa una fecha de partida", result);
	}
	

	

}
